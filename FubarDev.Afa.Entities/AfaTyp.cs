﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FubarDev.Afa.Entities
{
    public enum AfaTyp
    {
        Zuschreibung = 'Z',
        Abschreibung = 'A',
        Normal = 'X',
    }
}
