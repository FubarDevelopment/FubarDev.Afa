﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FubarDev.Afa.Entities
{
    public enum Abschreibungsart
    {
        Linear = 'L',
        Degressiv = 'D',
        GWG = 'G',
    }
}
